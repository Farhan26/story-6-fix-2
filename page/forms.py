from django import forms
from .models import Status


class StatusForm(forms.ModelForm):
    status = forms.CharField(max_length=250, widget=forms.Textarea)

    class Meta:
        model = Status
        fields = ('status',)
        label = {'status':'Status'}
        widgets = {
            'status':forms.Textarea(
                attrs = {
                    'class' : 'form-control',
                    'placeholder':'Apa yang sedang anda lakukan/rasakan saat ini?',
                }
            )
        }
