from django.db import models
from django.conf import settings
from django.utils import timezone

class Status(models.Model):
    status = models.TextField(max_length=250)
    
    def __str__(self):
        return self.status
    
