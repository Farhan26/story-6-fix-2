from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'page'

urlpatterns = [
    path('', views.redirecting, name='redirect'),
    path('home/', views.page, name='home'),
]
