from django.shortcuts import render, redirect
from django.views import generic
from page.forms import StatusForm
from page.models import Status


def page(request):
    if request.method == "POST":
        form = StatusForm(request.POST or None)
        if form.is_valid():
            status = form.save()
            status.save()
            return redirect("/home/")
    else:
        form = StatusForm()

    objectStatus = Status.objects.all()

    
    context = {
        'status':objectStatus,
        'page_title':'Status',
        'form':form,
    }

    return render(request, 'hmpage.html', context)


def redirecting(request):
    return redirect('/home/')
